#include "iostream"
#include "ros/ros.h"
#include "std_msgs/Int32.h"

void recieve(const std_msgs::Int32 msg)
{
    //std::cout << "subscriber: data: " << msg.data << std::endl;
    ROS_INFO("%s: data: %d\n", ros::this_node::getName(), msg.data);
    return;
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "subscriber");
    ros::NodeHandle n;
    ros::Subscriber listener = n.subscribe("/topic", 1000, recieve);
    ros::spin();
    return 0;
}