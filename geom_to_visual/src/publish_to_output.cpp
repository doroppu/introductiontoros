#include <ros/ros.h>
//#include <geometry_msgs/Point.h>
#include <visualization_msgs/Marker.h>

class SubscribeAndPublish
{
public:
    SubscribeAndPublish()
{
    //Topic you want to publish
    pub_ = n_.advertise<visualization_msgs::Marker>("pt_topic", 1);

    //Topic you want to subscribe
    sub_ = n_.subscribe("/8 8input", 1, &SubscribeAndPublish::callback, this);
}

void callback(const geometry_msgs::Point& input)
{
    visualization_msgs::Marker marker;

    marker.header.frame_id = "/my_frame";
    marker.header.stamp = ros::Time::now();
    marker.ns = "basic_shapes";
    marker.id = 0;
    marker.type = visualization_msgs::Marker::CUBE;
    marker.action = visualization_msgs::Marker::ADD;
    marker.pose.position.x = 0;
    marker.pose.position.y = 0;
    marker.pose.position.z = 0;
    marker.pose.orientation.x = 0.0;
    marker.pose.orientation.y = 0.0;
    marker.pose.orientation.z = 0.0;
    marker.pose.orientation.w = 1.0;
    marker.scale.x = 1.0;
    marker.scale.y = 1.0;

    marker.points.push_back(input);
    pub_.publish(marker);
  }

private:
    ros::NodeHandle n_; 
    ros::Publisher pub_;
    ros::Subscriber sub_;

};

int main(int argc, char **argv)
{
    ros::init(argc, argv, "subscribe_and_publish");
    SubscribeAndPublish SAPObject;
    ros::spin();
    return 0;
}
