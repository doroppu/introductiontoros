#include <ros/ros.h>
#include <geometry_msgs/PointStamped.h>
#include <tf/transform_listener.h>

class SubscribeAndPublish
{
public:
    SubscribeAndPublish()
{
    pub_ = n_.advertise<geometry_msgs::Point>("output", 1);
    sub_ = n_.subscribe("input", 1, &SubscribeAndPublish::callback, this);
}

void callback(const geometry_msgs::PointStamped& input)
{
    try{
        geometry_msgs::PointStamped transformed_pt;
        listener.transformPoint("core_frame", input, transformed_pt);

        geometry_msgs::Point pt;
        pt.x = transformed_pt.point.x;
        pt.y = transformed_pt.point.y;
        pt.z = transformed_pt.point.z;

        pub_.publish(pt);

    } catch (tf::TransformException& ex){
        ROS_ERROR("Error:", ex.what());        
    }
 }

private:
    ros::NodeHandle n_; 
    ros::Publisher pub_;
    ros::Subscriber sub_;
    tf::TransformListener listener;
    
};

int main(int argc, char **argv)
{
    ros::init(argc, argv, "tf_publish");
    SubscribeAndPublish SAPObject;
    ros::spin();
    return 0;
}
