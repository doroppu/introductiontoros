# IntroductionToROS

Sample code and tasks from the course **[Введение в Robot Operating System](https://stepik.org/course/3222)**.

Detailed tasks are described in the **[wiki](https://gitlab.com/drophead/introductiontoros/wikis/home)**.